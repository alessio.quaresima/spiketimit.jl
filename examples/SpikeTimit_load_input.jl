include("../src/SpikeTimit.jl")

# path = "/home/cocconat/Documents/Research/phd_project/speech/litwin-kumar_model_thesis/Spike TIMIT"
path = "/home/cocconat/Documents/Research/phd_project/speech/datasets/Spike TIMIT"

#Create the path strings leading to folders in the data set
test_path = joinpath(path, "test");
train_path = joinpath(path, "train");
dict_path = joinpath(path, "DOC/TIMITDIC.TXT");
train_set = SpikeTimit.create_dataset(;dir= train_path)
test_set = SpikeTimit.create_dataset(;dir= test_path)
dict = SpikeTimit.create_dictionary(file=dict_path)
##

# Parameters to compute the input_data
target_dialects = [1,2,5,8]
target_gender = "f" # "fm" "m"
samples = 10
n_speakers = 1
repetitions = 75 # amount of times you present the network with each unique stimulus.
silence_time = 0.15 # in seconds
n_features = 10 # number of features combined from input frequencies
words = ["that", "she"]
## Select a subset of the whole dataset.
# In this case I select all the female speakers from
# regional accent 1 that use at least on of the words in their
## I declared these functions because I am don't know how to use the Dataframes quesry properly *_*...
using DataFramesMeta
in_words(df_words) = !isempty(intersect(Set(df_words),Set(words)))
in_dialect(df_dialect) = df_dialect ∈ target_dialects
in_gender(df_gender) = occursin(df_gender, target_gender)

# this is a DataFrameMeta macro
speaker = @where(train_set,in_dialect.(:dialect), in_gender.(:gender), in_words.(:words))

##
## Select the inputs
durations, spikes, labels = SpikeTimit.select_words(speaker, words, samples = samples, encoding = "bae");

# SpikeTimit.resample_spikes!(spikes,n_feat=8)
# SpikeTimit.transform_into_bursts!(spikes)

spikes
##
## Mix them, if you like you can mix differently. Look at the function, it's simple!
all_ft, all_n, words_t, phones_t = SpikeTimit.mix_inputs(spikes, durations, labels, repetitions=repetitions, silence_time=silence_time)
SpikeTimit.transcriptions_to_dt!(words_t, 0.1)
SpikeTimit.transcriptions_to_dt!(phones_t, 0.1)
SpikeTimit.all_ft_to_dt!(all_ft, 0.1)
##

words_savepoints = SpikeTimit.get_savepoints(trans= words_t, n_measure=10)
ph_savepoints = SpikeTimit.get_savepoints(trans= phones_t, n_measure=10)
phones_t


## Comparing the last firing time, the duration of all words and the
## intervals of the words and phonemes we expect that it's well done!
all_ft[end]
input_length = repetitions*(sum(durations)+(silence_time*(length(durations))))-silence_time
words_t.intervals[end]
phones_t.steps[end]
