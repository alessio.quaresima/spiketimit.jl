using Revise
using DataFramesMeta
import SpikeTimit
using Random
using YAML

conf = YAML.load_file(joinpath(@__DIR__, "../conf/paths.yml"))
path_dataset = conf["dataset_path"]

ispath(path_dataset)
train_path = joinpath(path_dataset, "train");
dict_path = joinpath(path_dataset, "DOC", "TIMITDIC.TXT");
train = SpikeTimit.create_dataset(;dir= train_path)
dict = SpikeTimit.create_dictionary(file=dict_path)

##
all_words = vcat(train.sentence...)
using DataStructures
occurences = collect(counter(all_words))
ordered = sort(occurences, by=x->x[2],rev=true)
words = [x[1] for x in ordered[1:10]]
##

# The dataset:
names(train)
words

inputs = SpikeTimit.InputParams(dialects=[1], samples=10, gender=['m'], 
				 repetitions=10, shift_input=2, encoding="bae")

# Filter the dataset and retrieve network inputs
filtered_df = filter(:words => x-> any([word in x for word in inputs.words]), train) |> df -> filter(:dialect => x->x ∈ inputs.dialects, df) |> df -> filter(:gender => x->x ∈ inputs.gender, df)

filtered_df

# Get spikes, if samples < occurrences, pick a random subset of the occurrences
word_inputs = SpikeTimit.select_words(filtered_df, samples=inputs.samples, inputs.words, encoding= inputs.encoding )
if inputs.encoding == "bae"
	SpikeTimit.resample_spikes!(word_inputs.spikes)
	SpikeTimit.transform_into_bursts!(word_inputs.spikes)
end


keys(word_inputs)

word_inputs.durations[1]
counter(my_words)
length(word_inputs.durations)

SpikeTimit.raster_plot(word_inputs.spikes[1])

# Mix the inputs
ids = SpikeTimit.mix_inputs(length(word_inputs.durations), inputs.repetitions)
my_words = [word.word for word in word_inputs.labels[ids]]


spikes, transcriptions=  SpikeTimit.get_ordered_spikes(word_inputs, ids=ids, silence_time=inputs.silence_time, shift = inputs.shift_input)

spikes.neurons

keys(spikes)
keys(transcriptions)
fieldnames(typeof(transcriptions.words))

transcriptions.words.intervals[201]
transcriptions.words.signs[1]
length(transcriptions.phones.intervals)


# Flatten the spikes for the simulation loop

## Set the input projections parameters
projections = LKD.ProjectionParams(
		npop = SpikeTimit.get_encoding_dimension(word_inputs.spikes))

## Parameters to define network structure
weights_params = LKD.WeightParams()

## Set simulation details
last_interval = ceil(transcriptions.phones.intervals[end][end]*1000)
net = LKD.NetParams(simulation_time = last_interval, learning=true)


## Create network
W, popmembers = LKD.create_network(weights_params, projections)

spikes.neurons


## Get savepoints for words and phones
#@TODO shall we have them continuo beyond the word duration?

LKD.save_network(popmembers, W, folder)
## Simulate

transcriptions = SpikeTimit.transcriptions_dt(transcriptions)
spikes = SpikeTimit.ft_dt(spikes)
LKD.sim(W, popmembers, spikes, transcriptions, net, store, weights_params, projections)
## Read network properties
LKD.read_network_weights(folder)
