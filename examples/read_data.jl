### A Pluto.jl notebook ###
# v0.12.21
using SpikeTimit

# ╔═╡ c0d57c40-8010-11eb-3004-195f0590db26
md"""
This notebook will show how to import data from SpikeTimit[1] database and run it as a stimulus for the LKD network[2]. The SpikeTimit.jl module import the standard daataset released with the publication [1].

"""

# ╔═╡ 62beb3c0-8011-11eb-1ab5-8de2f870d0b2
md""" Import the module and the relevant packages"""

# ╔═╡ 8015ec4a-8011-11eb-04d6-9bcd09fada86
PATH = joinpath(@__DIR__,"SpikeTimit.jl")

# ╔═╡ 5e4f6080-8063-11eb-39b1-ab78ccdbf423
include(PATH)

# ╔═╡ 24e8c3e2-8064-11eb-23dc-2f6848c499e5


# ╔═╡ 693fb58a-8063-11eb-3e59-c9249009d1d6


# ╔═╡ 4566f194-8012-11eb-39d7-ad467788a78b
md"""
Import the dataset. Notice, the spike-time are not imported, only the files are stored and ready to be read. You have to set your PATH fort the dataset
"""

# ╔═╡ 42f1c31e-8063-11eb-0059-75ffb8aa555a
begin
	test_path = joinpath(@__DIR__,"Spike TIMIT", "test" );
	train_path = joinpath(@__DIR__,"Spike TIMIT", "train" );
	dict_path = joinpath(@__DIR__,"DOC","TIMITDIC.TXT");
end

# ╔═╡ 39885efc-8064-11eb-071d-c1eaa5f8892b


# ╔═╡ d3e653d0-8063-11eb-15e8-019ae2ff331a
md""" dict is a list of all words with the correesponding phones."""

# ╔═╡ 204518b2-8012-11eb-09f7-1f5da1ba4e1d
begin
	train = SpikeTimit.create_dataset(;dir= train_path)
	test = SpikeTimit.create_dataset(;dir= test_path)
	dict = SpikeTimit.create_dictionary(file=dict_path)
end


# ╔═╡ ae1af4c8-8011-11eb-0797-8d37104dcef5
md"""Select a subset of the dataset. Here for convenience I created a query to find all the sentences that contain the word "spring" in the train set.
ou can look up at the query and do others on the base of the DataFrame style. I suggest you to use the @linq macro and read the documentation carefully."""


# ╔═╡ f24740dc-8063-11eb-309d-e578fe133f5b
d_word = SpikeTimit.find_word(word="spring", df=train)

# ╔═╡ 4504b352-8011-11eb-0b7b-e97d88db44c9

md"""
Obviously, you can also choose the sentences by selecting some specific rows. Careful, the dataset has not an ordering.
"""

# ╔═╡ 40474e38-8011-11eb-0a99-917702896ff5
d_number = train[1,:]

# ╔═╡ 2ed1509a-8011-11eb-243e-e5df7b658803
md"""
Once you have sub-selected some columns, you can import the spike times. they are already corrected as explained in the PDF"""


# ╔═╡ 570de36a-8064-11eb-3bcb-076383a55a63
spikes= SpikeTimit.get_spiketimes(df=d_number)

# ╔═╡ 5c4b7dae-8064-11eb-1e77-b55a9c0588c0
plt1 = SpikeTimit.raster_plot(spikes[1])
plt1.plt

md"""
Also, the dataframe contains these fields:
speaker : the ID of the speaker
senID : the ID of the sentence
path : the path to access it (so you can retrieve the correct file adding the .EXT )
words : the words and their respective timing in ms
phones : the phones and their respective timing in ms

You can access it in this way:
"""

speakerID_firstsent = train[1,:speaker]
words_firstsent = train[1,:words]


# ╔═╡ 02a71dd8-8011-11eb-3f32-bb85b0c102f5
md"""
References
----------

[1] _Pan, Zihan, Yansong Chua, Jibin Wu, Malu Zhang, Haizhou Li, and Eliathamby Ambikairajah. “An Efficient and Perceptually Motivated Auditory Neural Encoding and Decoding Algorithm for Spiking Neural Networks.” Frontiers in Neuroscience 13 (2020). https://doi.org/10.3389/fnins.2019.01420._

[2] _Litwin-Kumar, Ashok, and Brent Doiron. “Formation and Maintenance of Neuronal Assemblies through Synaptic Plasticity.” Nature Communications 5, no. 1 (December 2014). https://doi.org/10.1038/ncomms6319._

"""



"""
Extract all the firing times and the corresponding neurons from an array with all the neurons and their relative firing times. i.e. the reverse_dictionary
"""
function reverse_dictionary(spikes)
	all_times = Dict()
	for n in eachindex(spikes)
		if !isempty(spikes[n])
			for t in spikes[n]
				tt = round(Int,t*10000)
				if haskey(all_times,tt)
					all_times[tt] = [all_times[tt]..., n]
				else
					push!(all_times, tt=>[n])
				end
			end
		end
	end
	return all_times
end

"""
From the reverse_dictionary data structure obtain 2 arrays that are faster to access in the simulation loop.
1. First array contains the sorted spike times.
2. The second array contains vectors with each firing neuron
"""
function sort_spikes(dictionary)
	neurons = Array{Vector{Int}}(undef, length(keys(dictionary)))
	sorted = sort(collect(keys(dictionary)))
	for (n,k) in enumerate(sorted)
		neurons[n] = dictionary[k]
	end
	return sorted, neurons
end


dictionary = reverse_spiketimes(spikes[1])
sorted, neurons = sort_spikes(dictionary)

## This will count all the firing event happened
firing_index= 1
next_firing_time= sorted[firing_index]

# This is the loop in the simulation
for tt in 1:10000
	if tt == next_firing_time
		firing_neurons = neurons[firing_index]
		firing_index +=1
		next_firing_time = sorted[firing_index]
		println("At time step: ", tt)
		println("These neurons fire: ", firing_neurons)
	end
end
