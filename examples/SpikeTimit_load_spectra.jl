using PyCall
using DataFrames
using DataFramesMeta
using Pandas
using YAML

cd(joinpath(@__DIR__,".."))
print(pwd())
##
py"""
import sys
import os
sys.path.insert(0, os.getcwd())
print(sys.path)
"""
##
TIMIT = pyimport("TIMIT_loader")
pyimport("importlib")."reload"(TIMIT)

conf = YAML.load_file(joinpath(@__DIR__, "../conf/paths.yml"))
path = conf["dataset_path"]

#path = "C:\\Users\\leoni\\Desktop\\3rd_year_AI\\1_Thesis\\litwin-kumar_model_thesis\\Spike TIMIT"
# path = "/home/cocconat/Documents/Research/phd_project/speech/litwin-kumar_model_thesis/Spike TIMIT"
@assert ispath(path)
dataset = TIMIT.create_dataset(joinpath(path,"train"))
spkrinfo, spkrsent = TIMIT.create_spkrdata(path)

# dataset |> Pandas.DataFrame |> DataFrames.DataFrame

##
#Create the path strings leading to folders in the data set
test_path = joinpath(path, "test");
train_path = joinpath(path, "train");
dict_path = joinpath(path, "DOC/TIMITDIC.TXT");
train_set = SpikeTimit.create_dataset(;dir= train_path)
test_set = SpikeTimit.create_dataset(;dir= test_path)
dict = SpikeTimit.create_dictionary(file=dict_path)


##

words = ["that"]
target_dialects = [1]
target_gender = "m" # "fm" "m"
in_words(df_words) = !isempty(intersect(Set(df_words),Set(words)))
in_dialect(df_dialect) = df_dialect ∈ target_dialects
in_gender(df_gender) = occursin(df_gender, target_gender)

# this is a DataFrameMeta macro
speaker = @where(train_set,in_dialect.(:dialect), in_gender.(:gender), in_words.(:words))
speaker.words
words = TIMIT.get_spectra(speaker |> Pandas.DataFrame, target_words=["that"])
##



using StatsBase
## For each phone, transform the spectrogram into 
# the average spectrogram by computing the mean over time for each frequency

function rate_coding_word(word::SpikeTimit.Word)
    times = []
    encoding = Matrix{Float64}(undef, 20, length(word.phones))
    for (n,ph) in enumerate(word.phones)
        encoding[:,n] = mean(ph.db, dims=2)[:,1]
        push!(times, ph.t0 - word.t0)
    end
    return times, encoding
end

using Plots
# times, phs = rate_coding_word(words[1])
words =SpikeTimit.py2j_words(words)

# for word in words 
#     word_spectrum = 
#     save(word_spectrum, "the path I like for the word")
# end


function get_my_encoding_from_spectrogram(spectrogram)
    spiketimes = Spiketimes()
    for frequency in eachrow(spectrogram)
        spikes_x_freq = get_spikes_from_frequency(frequency)
        push!(spikes_x_freq, spiketimes)
    end    
    return spiketimes
end

function get_spikes_from_frequency(frequency::Array{float64})
    spikes = []
    for (n,value) in enumerate(frequency)
        #value:: db
        rate =  value * scaling_factor
        if Poisson(rand(rate))
            push!(spikes, n*1/sampling_rate)
        end
    end
    return spikes
end

count_spikes(spiketimes[])

spiketimes = get_my_encoding_from_spectrogram(spectrogram)



##
p = plot(vcat([ph.osc for ph in words[1].phones]), title=words[1].word)
spikes = SpikeTimit.get_spiketimes(df=speaker[1:1,:], encoding="cochlea70")[1
q = SpikeTimit.raster_plot(spikes)
plot(p,q, layout=(2,1))



##
words[1]
a = heatmap(words[1].phones[2].db, title=words[1].phones[2].ph);
b = heatmap(words[3].phones[2].db, title=words[1].phones[2].ph);
c = heatmap(words[2].phones[2].db, title=words[1].phones[2].ph);
d = heatmap(words[4].phones[2].db, title=words[1].phones[2].ph);
words[1].word
Plots.plot(a,b,c,d, layout=(4,1), colorbar=false)

a = Plots.plot(words[1].phones[2].osc, title=words[1].phones[2].ph);
b = Plots.plot(words[3].phones[2].osc, title=words[1].phones[2].ph);
c = Plots.plot(words[2].phones[2].osc, title=words[1].phones[2].ph);
d = Plots.plot(words[4].phones[2].osc, title=words[1].phones[2].ph);
##
words[1].phones[2].ph
times, phs = rate_coding_word(words[9])
heatmap(phs)
words[1].phones[1].ph
