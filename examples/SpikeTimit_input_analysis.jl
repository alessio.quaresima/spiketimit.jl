#this file is part of litwin-kumar_doiron_formation_2014
#Copyright (C) 2014 Ashok Litwin-Kumar
#see README for more information


#==
This file looks into the poperties of the SpikeTimit dataset.
I will:
1. Import the train set.
2. Select a subspace of the dataset with fixed:
	regional accent (dr1)
	gender (f)
	set of words ()
	set of words (["that", "had", "she", "me", "your", "year"])

3. Show the raster plot of a single realization of these words in the dataset

4. Train a classifier to distinguish between these words on a reduced feature space.

5. Expand the dataset to 4 regional accents and both genders and
	extract the time of the phones that compose the word "year"

6. Train a classifier to distinguish between these phones on a reduced feature space.

7. Extract an average map of these phones.

==#
using Plots
using Random
using StatsBase


## 1
include("../src/SpikeTimit.jl")
path = "/home/cocconat/Documents/Research/phd_project/speech/datasets/Spike TIMIT"
test_path = joinpath(path, "test");
train_path = joinpath(path, "train");
dict_path = joinpath(path,"DOC", "TIMITDIC.TXT");
train_set = SpikeTimit.create_dataset(;dir= train_path)
test_set = SpikeTimit.create_dataset(;dir= test_path)
dict = SpikeTimit.create_dictionary(file=dict_path)

## 2
Dong_words = ["that", "had", "she", "me", "your", "year"]
speakers = []
target_dialects = [1,2,3]
in_dialect(df_dialect) = df_dialect ∈ target_dialects
in_gender(df_gender) = occursin(df_gender, target_gender)
for word in Dong_words
	word = @linq train_set |>
			where(in_dialect.(:dialect), :gender.=='f', word .∈ :words) |>
		 	select(:speaker) |> unique
		@show word
	push!(speakers,Set(word.speaker))
end
target_speakers = collect(intersect(speakers...))
in_speaker(df_speaker) = df_speaker ∈ target_speakers
target_dialects
in_speaker("fsjk1")
single_speaker_train = @where(train_set,:speaker .==target_speakers[1])
multi_speaker_train = @where(train_set,in_speaker.(:speaker))
## 3

spikes = []
plots = []
encoding="bae"
for word in Dong_words
	# spiketimes, duration, phones = SpikeTimit.get_word_spikes(;df=single_speaker_train, word=word)
	spiketimes, durations = SpikeTimit.get_word_spikes(single_speaker_train, word, encoding=encoding)
	@show typeof(spiketimes)
	SpikeTimit.resample_spikes!(spiketimes, n_feat=8)
	push!(spikes,spiketimes[1])
	plt=SpikeTimit.raster_plot(spiketimes[1])
	push!(plots,plt)
end

Plots.plot(plots...)
##
spikes = []
plots = []
encoding="cochlea70"
labels= Vector{Int}()
for (n,word) in enumerate(Dong_words)
	# spiketimes, duration, phones = SpikeTimit.get_word_spikes(;df=single_speaker_train, word=word)
	spiketimes, durations = SpikeTimit.get_word_spikes(multi_speaker_train, word, encoding=encoding)
	push!(spikes,spiketimes...)
	push!(labels,repeat([n],length(spiketimes))...)
	plt=SpikeTimit.raster_plot(spiketimes[1])
	Plots.plot!(title=word)
	push!(plots,plt)
end

spikes
Plots.plot(plots...)

## 4. Compute average time of each word

data = zeros(length(spikes),length(spikes[1]))
for st in eachindex(spikes)
	for n in eachindex(spikes[st])
		if !isempty(spikes[st][n])
			data[st,n] =sum(spikes[st][n].>0)
		end
	end
	#
end


plots = []
for n = 1:6
	plt = scatter(data[n,:], label=false)
	push!(plots,plt)
end
Plots.plot(plots...)


## 4.bis Compute the distinguishability

samples = 2000

data
labels


## 4 Define classifier


SpikeTimit.MultinomialLogisticRegression(copy(data'),labels)

## Select a subset of regional dialect and the word "year".
target_dialects = [1]
in_dialect(df_dialect) = df_dialect ∈ target_dialects
single_dialect = @where(train_set,in_dialect.(:dialect))
df = SpikeTimit.find_word(words=["year"],df=single_dialect)


## Do Phone stuff,  not relevant now!

## Get spectra

words = []
for word in Dong_words
	_words = TIMIT.get_spectra(multi_speaker_train |> Pandas.DataFrame, target_words=[word])
	push!(words, SpikeTimit.py2j_words(_words))
end

my_words = [w for w in words]



## Get the time intervals of all the phones contained in the word, within the dataset
spikes = SpikeTimit.get_spiketimes(;df=df,encoding="cochlea70")
df.phones

SpikeTimit.get_spikes_in_interval(spikes[1],[[0.3,0.2]])
# SpikeTimit.resample_spikes(;spiketimes=spikes, n_feat=2)
phone_labels, spiketimes = get_phones_spiketimes(spikes, phones)

## Compute the mean for each phone and set the classification stage
get_mean(x) = isempty(x) ? 0. : mean(x)
data = map(neuron -> get_mean.(neuron), spiketimes)
all_labels = collect(Set(phone_labels))
train_dataset = zeros(length(data[1]), 1000)
labels= zeros(Int,1000)
for x in 1:1000
	n = rand(1:length(data))
	phone = phone_labels[n]
	train_dataset[:,x] .= data[n]
	labels[x] = findfirst(l-> phone .== l, all_labels)
end

MultinomialLogisticRegression(train_dataset,labels)

## Show the average qualities of all the phones

plots=[]
_labels=[]
for n = 1:length(all_labels)
	realizations = findall(l-> all_labels[n] .== l, phone_labels)
	if length(realizations) > 5
		traceplot = SpikeTimit.TracePlot(length(spiketimes[1]), alpha=1/length(realizations))
		plot(traceplot.plt, title=all_labels[n])
		push!(plots, traceplot)
		push!(_labels, all_labels[n])
	end
end
plots
	# plots = repeat([traceplot],length(all_labels))
for (_l, ax) in zip(_labels, plots)
	realizations = findall(l-> _l .== l, phone_labels)
	@show length(realizations)
	for r in realizations
		SpikeTimit.raster_plot(spiketimes[r], ax=ax, alpha=0.1)
	end
end

plot([p.plt for p in plots]..., legend=false, alpha=0.0001)
