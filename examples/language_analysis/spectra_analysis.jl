using PyCall
using DataFrames
using DataFramesMeta
using Pandas

cd(joinpath(@__DIR__,".."))
py"""
import sys
import os
sys.path.insert(0, os.getcwd())
print(sys.path)
"""
TIMIT = pyimport("TIMIT_loader")
pyimport("importlib")."reload"(TIMIT)
##

#path = "C:\\Users\\leoni\\Desktop\\3rd_year_AI\\1_Thesis\\litwin-kumar_model_thesis\\Spike TIMIT"
path = "/home/cocconat/Documents/Research/phd_project/speech/litwin-kumar_model_thesis/Spike TIMIT"

dataset = TIMIT.create_dataset(joinpath(path,"train"))
spkrinfo, spkrsent = TIMIT.create_spkrdata(path)


##
include("../src/SpikeTimit.jl")

#Create the path strings leading to folders in the data set
test_path = joinpath(path, "test");
train_path = joinpath(path, "train");
dict_path = joinpath(path, "DOC/TIMITDIC.TXT");
train = SpikeTimit.create_dataset(;dir= train_path)
test = SpikeTimit.create_dataset(;dir= test_path)
dict = SpikeTimit.create_dictionary(file=dict_path)


##


const NYC = 6
const SOUTH = 5
const male = "m"
const female = "f"
##
words = ["that", "had", "she", "me", "your", "all", "like", "don't", "year", "water", "dark", "rag", "oily", "wash", "ask", "carry", "suit"]
# nyc_male = @where(train,in_dialect.(:dialect, target=NYC), in_gender.(:gender, target=male), in_words.(:words, target=words))
# nyc_female = @where(train,in_dialect.(:dialect, target=NYC), in_gender.(:gender, target=female), in_words.(:words, target=words))
# south_male = @where(train,in_dialect.(:dialect, target=SOUTH), in_gender.(:gender, target=male), in_words.(:words, target=words))
# south_female = @where(train,in_dialect.(:dialect, target=SOUTH), in_gender.(:gender, target=female), in_words.(:words, target=words))
##
##
##
# words[1].phones[1].db
using StatsBase
using Plots

function merge_phones(phone1, phone2)
    ph = Phone(phone1.ph*phone2.ph,phone1.t0, phone2.t1 )
	ph.osc = vcat(phone1.osc, phone2.osc)
	ph.db = vcat(phone1.db, phone2.db)
	return ph
end

function get_all_phones(dataset, words)
	all_phones = Dict()
	isa(words, String) && (words = [words])
	for word in words
		matches = SpikeTimit.find_word(;df=dataset, word=word) |> x-> TIMIT.get_spectra(x |> Pandas.DataFrame, target_words=word)
		for word in SpikeTimit.py2j_words(matches)
		    # println("\n")
		    for phone in word.phones
		        # print(phone.ph," ")
		        if haskey(all_phones,phone.ph)
		            push!(all_phones[phone.ph], phone)
		        else
		            push!(all_phones,phone.ph=>[phone])
		        end
		    end
		end
	end
	return all_phones
end

function padding(phone; pad_length=400, pad_zero = -80)
	# mat = zeros(20, pad_length)
	mat = ones(20, pad_length) .* (-80)
	if size(phone.db)[2]> 400
		mat[:,1:400] .= phone.db[:,1:400]
	else
		mat[:,1:size(phone.db)[2]] .= phone.db[:,1:end]
	end
	return mat
end

function get_padded_features(all_phones)
	phones = Vector{String}(undef, length(all_phones))
	cases  = Vector{Int}(undef, length(all_phones))
	features = Vector{Matrix{Float64}}(undef, length(all_phones))
	phones_labels = collect(keys(all_phones))
	Threads.@threads for idx in 1:length(phones_labels)
		ph = phones_labels[idx]
		samples = length(all_phones[ph])
		phones[idx]=  ph
		cases[idx] = samples
		data_phone = zeros(400*20, samples)
		for n in 1:samples
			data_phone[:, n] .= padding(all_phones[ph][n])[:]
		end
		features[idx] = data_phone
	end
	return phones, cases, features
end

function compare_sounds(samples1, samples2)
	all_phones = []
	push!(all_phones, samples1[1]...)
	push!(all_phones, samples2[1]...)
	all_phones = collect(Set(all_phones))
	measures = zeros(length(all_phones),2)
	for (n,ph) in enumerate(all_phones)
		ex1 = findfirst(x->x==all_phones[n],samples1[1])
		ex2 = findfirst(x->x==all_phones[n],samples2[1])
		measures[n,1] = isnothing(ex1) ? 1 : samples1[2][ex1]
		measures[n,2] = isnothing(ex2) ? 1 : samples2[2][ex2]
	end
	return 	jsd(measures[:,1], measures[:,2])
end

jsd(x,y)= 0.5*(kldivergence(x, y) + kldivergence(y,x))


	# crossentropy(dr1, dr2)



##
males=[]
females=[]
for dr in 1:8
	push!(males,@where(train,in_dialect.(:dialect, target=dr), in_gender.(:gender, target=male), in_words.(:words, target=words)) |> x->get_padded_features(get_all_phones(x, words)))
	push!(females,@where(train,in_dialect.(:dialect, target=dr), in_gender.(:gender, target=female), in_words.(:words, target=words)) |> x->get_padded_features(get_all_phones(x, words)))
end

# ##
# data = [males..., females...]
# entropy = zeros(16,16)
# for x in  1:16
# 	for y in 1:16
# 		entropy[x,y] = compare_sounds(data[x], data[y])
# 	end
# end
# hclust(entropy)
# hclust(entropy)
# # heatmap(entropy)
##
all_sounds=[]
for (sounds, samples, _) in data
	push!(all_sounds, sounds)
end
common_sounds = collect(intersect(Set.(all_sounds)...))


mean_data = zeros(8000, 16, length(common_sounds))
std_data = zeros(8000, 16, length(common_sounds))
for (n,s) in enumerate(common_sounds)
	speaker=0
	for (sounds, samples, feats) in data
		speaker+=1
		id = findfirst(x->x==s, sounds)
		mean_data[:, speaker, n] = mean(feats[id], dims=2)[:,1]
		std_data[:, speaker, n] = std(feats[id], dims=2)[:,1]
	end
end

# heatmap(reshape(std_data[:,16,10],20,400))
# heatmap(reshape(mean_data[:,16,10],20,400))

##
phone_div =zeros(35,35,2)
for speaker in 1:16
	for x in 1:35
		for y in 1:35
			phone_div[x,y,1]+=jsd(abs.(mean_data[:,speaker,x]),abs.(mean_data[:,speaker,y]))
			phone_div[x,y,2]+=jsd(abs.(std_data[:,speaker,x]),abs.(std_data[:,speaker,y]))
		end
	end
end
speaker_div =zeros(16,16,2)
for phone in 1:35
	for spx in 1:16
		for spy in 1:16
			speaker_div[spx,spy,1]+=jsd(abs.(mean_data[:,spx,phone]),abs.(mean_data[:,spy,phone]))
			speaker_div[spx,spy,2]+=jsd(abs.(std_data[:,spx,phone]),abs.(std_data[:,spy,phone]))
		end
	end
end
##

using Clustering
drs = [
	"m_New England",
	"m_Northern",
	"m_North Midland",
	"m_South Midland",
	"m_Southern",
	"m_New York City",
	"m_Western",
	"m_Moved around",
	"f_New England",
	"f_Northern",
	"f_North Midland",
	"f_South Midland",
	"f_Southern",
	"f_New York City",
	"f_Western",
	"f_Moved around"]
drs= repeat(drs,2)
order = hclust(speaker_div[:,:,1],linkage=:complete).order
heatmap(speaker_div[order,order,1])
heatmap(speaker_div[order,order,2])
pdialects = plot!(xticks=(1:16,drs[order]),yticks=(1:16, drs[order]), xrotation=45, colorbar=false, tickfontsize=10, title="Dialects cross-entropy")
##
heatmap(phone_div)
heatmap(reshape(all_data[:,6,10], 20,400))
all_data[:,6,10]

order = hclust(phone_div, linkage=:complete).order
heatmap(phone_div[order,order])
common_sounds
pphones =plot!(xticks=(1:35,collect(common_sounds)[order]),yticks=(1:35, common_sounds[order]), xrotation=45, colorbar=false, tickfontsize=10, title="Phones cross-entropy")
##
savefig(pphones, "phones_similarities.png")
savefig(pdialects, "dialects_crossentropy.png")

##
##
variations =zeros(8000,16,35)
for phone in 1:35
	for sp in 1:16
		variations[:,sp, phone]= std
	end
end
#
data[1][3]
#
# mean(data[1][3][10], dims=2)[:,1]
# # , dims=2
# # , dims=2
#
# data[1][3][10]
# heatmap(map(x->reshape(x, 20,400), mean.(data[1][3][2], dims=2))[1])
# heatmap(map(x->reshape(x, 20,400), mean.(nyc_male_data[3], dims=2))[2])
# heatmap(map(x->reshape(x, 20,400), mean.(nyc_male_data[3], dims=2))[3])
# heatmap(map(x->reshape(x, 20,400), mean.(male_data[3], dims=2))[4])
# heatmap(map(x->reshape(x, 20,400), mean.(nyc_male_data[3], dims=2))[5])
# heatmap(map(x->reshape(x, 20,400), mean.(nyc_male_data[3], dims=2))[6])
# heatmap(map(x->reshape(x, 20,400), mean.(nyc_male_data[3], dims=2))[35])
##
phone_div =ones(16,16,35,35)*-1
for sp1 in 1:16
	for sp2 in 1:sp1
		for x in 1:35
			for y in 1:x
				phone_div[sp1,sp2,x,y]=jsd(abs.(all_data[:,sp1,x]),abs.(all_data[:,sp2,y]))
				phone_div[sp2,sp1,y,x]= phone_div[sp1,sp2,x,y]
			end
		end
	end
end
##
heatmap(phone_div[1,:,4,:])
hclust(phone_div)
##
word = "little"
matches = SpikeTimit.find_word(;df=train, word=word)
water_f = @where(matches, SpikeTimit.in_gender.(:gender,target="f")) |> x-> TIMIT.get_spectra(x |> Pandas.DataFrame, target_words=word)[1]
water_m = @where(matches, SpikeTimit.in_gender.(:gender,target="m")) |> x-> TIMIT.get_spectra(x |> Pandas.DataFrame, target_words=word)[2]

function stack_phones(word)
	phones = []
	for ph in word.phones
		push!(phones,ph.db)
	end
	return phones
end
using Plots

p = Plots.plot(heatmap(hcat(stack_phones(water_f)...), title="Word: "*word),heatmap(hcat(stack_phones(water_m)...), ylabel="Frequency", xlabel="Duration (ms)"), layout=(2,1), colorbar=false, guidefontsize=18, tickfontsize=15, titlefontsize=20)
savefig(p,"word_little.png")

q = Plots.plot(heatmap(water_f.phones[4].db, title="Phone: EL"),heatmap(water_m.phones[4].db, ylabel="Frequency", xlabel="Duration (ms)"), layout=(2,1), colorbar=false, guidefontsize=18, tickfontsize=15, titlefontsize=20)
savefig(q,"phone_little.png")


##
heatmap(padding(all_phones["ae"][1]))
heatmap(padding(all_phones["ae"][2]))
heatmap(padding(all_phones["ae"][3]))



##

function rate_coding_word(word::SpikeTimit.Word)
    times = []
    encoding = Matrix{Float64}(undef, 20, length(word.phones))
    for (n,ph) in enumerate(word.phones)
        encoding[:,n] = mean(ph.db, dims=2)[:,1]
        push!(times, ph.t0 - word.t0)
    end
    return times, encoding
end

using Plots
times, phs = rate_coding_word(words[1])
a = heatmap(words[1].phones[1].db)
b = heatmap(words[1].phones[2].db)
c = heatmap(words[1].phones[3].db)
words[1].word
Plots.plot(a,b,c, layout=(1,3), colorbar=false, axes=nothing, ticks=nothing)
times, phs = rate_coding_word(words[9])
heatmap(phs)
words[1].phones[1].ph
