
## import the dataset. Notice, the spike-time are not imported, only the files are stored and ready to be read.
# using SpikeTimit
basedir = "/home/cocconat/Documents/Research/phd_project/speech/datasets/Spike TIMIT"
using Plots

test_path = joinpath(basedir, "test" )
train_path = joinpath(basedir,"train" )
dict_path = joinpath(basedir,"DOC","TIMITDIC.TXT")

dict = SpikeTimit.create_dictionary(file=dict_path)
train_set = SpikeTimit.create_dataset(;dir= train_path)
test_set = SpikeTimit.create_dataset(;dir= test_path)

##     function get_spiketimes(;df, word)

my_sent = train_set[121:121,:]
my_sent.words
#we want the dark word

spikes = SpikeTimit.get_spikes_cochlea(;df=my_sent, encoding=70)

using StatsBase
sum(length.(spikes[1])./2.5)

SpikeTimit.raster_plot(spikes[1])

plot!(xlims=(0., 0.001))


## Verify spike resampling is correct
##
for spike in spikes
	# spikes = SpikeTimit.resample_spikes(spike_times=spikes,n_feat=8)
	h = histogram(vcat(spike...),bins=0:0.01:3 )
	push!(plots,h)
end

plot(plots...)
sts, durations = SpikeTimit.get_spikes_in_interval(;spiketimes=spiketimes, intervals=intervals)
SpikeTimit.stack_spiketimes(sts, durations, 0.1)

isempty(Matrix{Float64}(undef,0,0))


intervals[1]

##


prompts = joinpath(@__DIR__,"DOC","PROMPTS.TXT")

function get_sentences(;prompts)
	dictionary=Dict()
	for line in readlines(prompts)
		if !startswith(line,";")
			words = split(line, " ")
			for word in words
				a = lowercase(replace(word,[',','.',';']=>""))
				if !haskey(dictionary,a)
					push!(dictionary, a=>1)
				else
					dictionary[a]+=1
				end
			end
		end
	end
	return dictionary
end

words = get_sentences(prompts=prompts)

sort(words, rev=true, by=values)
sort(collect(words), by=x->x[2])

# Select a subset of the dataset. Here for convenience I created a query to find all the sentences that contain the word "spring" in the train set.
# You can look up at the query and do others on the base of the DataFrame style. I suggest you to use the @linq macro and read the documentation carefully.
d_word = SpikeTimit.find_word(word="spring", df=train)

# Obviously, you can also choose the sentences by selecting some specific rows. Careful, the dataset has not an ordering.
d_number = train[1,:]

# Once you have sub-selected some columns, you can import the spike times.
# they are already corrected as explained in the PDF
spikes= SpikeTimit.get_spiketimes(df=d_number)


## Measure the spike frequency on a sample
counter = 0
for x in spikes[1]
	counter += length(x)
end
duration = d_number[:phones] |> phones->map(phone->phone[end], phones)

## Data from LDK
jex =1.78
rx_n = 4.5e3

g_noise  = jex*rx_n

spike_per_pop = counter/620/2.92 ## Hz per population

pmemb = 0.2
jex_s = 50
g_signal = spike_per_pop *620*pmemb*jex_s



## Now let's convert the spike-times into a convenient input.

test_path = "/run/media/cocconat/data/speech_encoding/Spike TIMIT/Spike TIMIT/test"
test = SpikeTimit.create_dataset(;dir= test_path)

## select two samples
d_number = test[1:2,:]
spikes= SpikeTimit.get_spiketimes(df=d_number)

## Show how it works for a single sample
dictionary = SpikeTimit.reverse_dictionary(spikes[1], 0.1)
sorted, neurons = SpikeTimit.sort_spikes(dictionary)


## check the length of these array is the same
all_ft, all_n = SpikeTimit.inputs_from_df(test, [1,2])


## This is the loop in the simulation, place it in the input section
firing_index= 1
next_firing_time= all_ft[firing_index]
for tt in 1:10000
	if tt == next_firing_time
		firing_neurons = all_n[firing_index]
		firing_index +=1
		next_firing_time = all_ft[firing_index]
		println("At time step: ", tt)
		println("These neurons fire: ", firing_neurons)
	end
end

using Plots
plot(length.(all_n))
