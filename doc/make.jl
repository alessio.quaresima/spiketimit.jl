using Documenter
cd(".")
include("../src/SpikeTimit.jl")
using .SpikeTimit

println("Module SpikeTimit imported")

makedocs(root="../", source="src", build="build",
         doctest=true, modules= Module[SpikeTimit],
         repo="", highlightsig=true, sitename="SpikeTimit",
         expandfirst=[], pages=[ "Index" => "index.md"])
