using Base

module SpikeTimit
    using DataFrames
    using DataFramesMeta
    using OrderedCollections
    using MAT
    using Plots
    using ColorSchemes
    ## This is the sample rate used in the scripts.
    sr = 16000
    ## This is the rescaling factor for the spike-time discussed in the PDF
    correction = 5.

    import Plots: Series, Plot, Subplot

	export find_word

    ## Create a dictionary with all words in the dataset
    function create_dictionary(;file)
        dict = OrderedDict()
        for line in readlines(file)
            if !startswith(line, ";")
                word, sounds = split(line,"/")
                # @show word, sounds
                push!(dict, word=>sounds)
            end
        end
        return dict
    end

    function get_words(root, senID)
      path = joinpath(root, senID*".wrd")
      times0 = []
      times1 = []
      words = []
      for line in readlines(path)
          t0,tf,w = split(line)
          push!(times0,parse(Int,t0))
          push!(times1,parse(Int,tf))
          push!(words, String(w))
      end
      _data = Array{Union{String,Float64},2}(undef,length(times0),3)
      _data[:,1] = words
      _data[:,2] = times0 ./ sr
      _data[:,3] = times1 ./ sr
      return _data
    end

    function get_phones(root, senID)
      path = joinpath(root, senID*".phn")
      times0 = []
      times1 = []
      phones = []
      for line in readlines(path)
          t0,tf,p = split(line)
          push!(times0,parse(Int,t0))
          push!(times1,parse(Int,tf))
          push!(phones, String(p))
      end
      _data = Array{Union{String,Float64},2}(undef,length(times0),3)
      _data[:,1] = phones
      _data[:,2] = times0 ./ sr
      _data[:,3] = times1 ./ sr
      return _data
    end
    function create_dataset(;dir)
        df = DataFrame(speaker = String[] , senID = String[], path=String[], words=Array{Union{String, Float64},2}[], phones=Array{Union{String, Float64},2}[])
        for (root, dirs, files) in walkdir(dir)
            for file in files
                if endswith(file,"wav")
                    speaker = split(root, "/")[end]
                    senID = split(file,".")[1]
                    words = get_words(root, senID)
                    phones = get_phones(root, senID)
                    # @show phones
                    push!(df,(speaker,senID,joinpath(root,senID),words,phones))
                end
            end
        end
        return df
    end

    """Search for word in the dataset and return the items"""
    function find_word(;word, df)
        return @linq transform(filter(:words => x-> word ∈ x , df), :senID) |> unique
    end

    function get_spiketimes(;df)
        get_spiketimes_file(file)= file*"_spike_timing.mat"
        if length(size(df)) == 1 # only 1 row
            spikes = [read(matopen(get_spiketimes_file(df.path)))["spike_output"][1,:]]
        else
            spikes = map(x->x[1,:],get_spiketimes_file.(df.path) |> x->matopen.(x) |> x-> read.(x) |> x->get.(x,"spike_output", nothing) )
        end
        map(spike->map(row->spike[row] = [spike[row]], findall(typeof.(spike) .==Float64)), spikes)
        map(spike->findall(isa.(spike,Matrix)) |> x->spike[x] = spike[x]*correction, spikes)
        return spikes
    end


    function get_matrix(;df)
        get_matrix_file(file) =  file*"_binary_matrix.mat"
        return get_matrix_file.(df.path)
    end

    function get_file(file, ext)
        return file*"."*ext
    end

    ## make fancy raste plot
    struct TracePlot{I,T}
        indices::I
        plt::Plot{T}
        sp::Subplot{T}
        series::Vector{Series}
    end
    function TracePlot(n::Int = 1; maxn::Int = typemax(Int), sp = nothing, kw...)
        clist= get(ColorSchemes.colorschemes[:viridis],range(0,1,length=n))
        indices = if n > maxn
            shuffle(1:n)[1:maxn]
        else
            1:n
        end
        if sp == nothing
            plt = scatter(length(indices);kw...)
            sp = plt[1]
        else
            plt = scatter!(sp, length(indices); kw...)
        end
        for n in indices
            sp.series_list[n].plotattributes[:markercolor]=clist[n]
            sp.series_list[n].plotattributes[:markerstrokecolor]=clist[n]
        end
        TracePlot(indices, plt, sp, sp.series_list)
    end

    function Base.push!(tp::TracePlot, x::Number, y::AbstractVector)
        push!(tp.series[x], y, x .*ones(length(y)))
    end
    Base.push!(tp::TracePlot, x::Number, y::Number) = push!(tp, [y], x)

    function raster_plot(spikes; alpha=0.5)
        s = TracePlot(length(spikes), legend=false)
        for (n,z) in enumerate(spikes)
            if length(z)>0
                push!(s,n,z[1,:])
            end
        end
        return s
    end
    ###########################################################


	"""
	Extract all the firing times and the corresponding neurons from an array with all the neurons and their relative firing times. i.e. the reverse_dictionary
	"""
	function reverse_dictionary(spikes,dt::Float64)
		all_times = Dict()
		for n in eachindex(spikes)
			if !isempty(spikes[n])
				for t in spikes[n]
					tt = round(Int,t*1000/dt) ## from seconds to timesteps
					if haskey(all_times,tt)
						all_times[tt] = [all_times[tt]..., n]
					else
						push!(all_times, tt=>[n])
					end
				end
			end
		end
		return all_times
	end



	"""
	From the reverse_dictionary data structure obtain 2 arrays that are faster to access in the simulation loop.
	1. First array contains the sorted spike times.
	2. The second array contains vectors with each firing neuron
	"""
	function sort_spikes(dictionary)
		neurons = Array{Vector{Int}}(undef, length(keys(dictionary)))
		sorted = sort(collect(keys(dictionary)))
		for (n,k) in enumerate(sorted)
			neurons[n] = dictionary[k]
		end
		return sorted, neurons
	end


	"""
	Stack together different input maps:
		- spikes is an array with inputs
		- duration is the duration in seconds of each encoding
	"""
	function stack_spiketimes(spikes, duration, dt)
		global_time_step = 0
		all_neurons = []
		all_ft = []
		for (spike_times,dd) in zip(spikes, duration)
			dictionary = reverse_dictionary(spikes[1], dt)
			sorted, neurons = sort_spikes(dictionary)
			#shift time:
			sorted .+= global_time_step

			## put them together
			all_neurons = vcat(all_neurons, neurons)
			all_ft = vcat(all_ft..., sorted)
			global_time_step += round(Int, dd*1000/dt)
		end
		return all_ft, all_neurons
	end

	"""
	Return the input of the spike trains corrsponding to the rows, in the (spike-time -> neuron ) format
	"""
	function inputs_from_df(df, rows; dt=0.1)
		_df = df[rows,:]
		## Get the duration of each frame: it corresponds to the (last) symbol h# in the phones array
		duration = df.phones |> phones->map(phone->phone[end], phones)
		spikes= SpikeTimit.get_spiketimes(df=_df)
		all_ft, all_n =  stack_spiketimes(spikes, duration, dt)
		@assert(size(all_ft) == size(all_n))
		return all_ft, all_n
	end

	"""
	For each word extract the interval corresponding to it
	"""
	function get_interval_word(;df, word::String)
		intervals = []
		for row in eachrow(df)
			interval = [0.,0.]
			for my_word in eachrow(row.words)
				if my_word[1] == word
					interval = my_word[2:3]
				end
			end
			push!(intervals, interval)
		end
		isempty(intervals)
		return intervals
	end

	"""
	Return the spiketimes subset corresponding to the selected interval, for each sentence
	"""
	function get_spikes_in_interval(; spiketimes, intervals)
		new_spiketimes = Vector()
		durations = Vector()
		for (spikes, interval) in zip(spiketimes, intervals)
			new_spikes, duration  = _get_spikes_in_interval(spikes, interval)
			push!(new_spiketimes, new_spikes)
			push!(durations, duration)
		end
		return new_spiketimes, durations
	end


	"""
	Return the spiketimes subset corresponding to the selected interval, for one sentence
	"""
	function _get_spikes_in_interval(spikes, interval)
		for n in eachindex(spikes)
			in_interval(x) = (x > interval[1])*(x < interval[end])
			spikes[n] = filter(in_interval,spikes[n]) .- interval[1]
		end
		return spikes, interval[end] - interval[1]
	end


	@doc """
	Unify frequencies bin
	Input: spiketimes array with 620 elements
	Return array with sorted spikes in less classes
	"""
	function resample_spikes(;spike_times, n_feat=7)
		FREQUENCIES = 20

		old_bins = length(spike_times)÷FREQUENCIES
		new_bins = 31÷n_feat+1
		new_spikes = map(x->Vector{Float64}(),1:new_bins*FREQUENCIES)

		for freq in 1:FREQUENCIES
			old_freq = (freq-1)*old_bins
			new_freq = (freq-1)*new_bins
			for new_bin in 1:new_bins
				last_bin = new_bin*n_feat <32 ? new_bin*n_feat : 31
				bins = 1+(new_bin-1)*n_feat : last_bin
				for old_bin in bins
					push!(new_spikes[new_bin+new_freq], spike[old_bin+old_freq]...)
				end
			end
		end
		return sort!.(new_spikes)
	end

end
