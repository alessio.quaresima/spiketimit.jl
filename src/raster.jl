#=======================
	 Raster Plot
=======================#

function raster_plot(exc::Spiketimes, inh1::Spiketimes, inh2::Spiketimes; ax=nothing, kwargs...)
	npop = [0, length(exc), length(inh1), length(inh2)]
	_x, _y = Float32[], Float32[]
    y0 = Int32[0]
	for (_n, pop) in enumerate([exc, inh1, inh2])
	    for n in eachindex(pop)
			for ft in pop[n]
				push!(_x,ft)
				push!(_y,n+cumsum(npop)[_n])
			end
		end
		push!(y0,npop[_n])
	end
    plt = scatter(_x, _y, m = (1, :black), leg = :none,
                  xaxis=("Time (s)", (0, Inf)), yaxis = ("neuron",))
    y0 = y0[2:end]
    !isempty(y0) && hline!(plt, cumsum(y0), linecolor = :red)
end

function raster_plot(pop::Spiketimes; ax=nothing, kwargs...)

	_x, _y = Float32[], Float32[]
    y0 = Int32[0]
    for n in eachindex(pop)
		for ft in pop[n]
			push!(_x,ft)
			push!(_y,n)
		end
	end
    plt = scatter(_x , _y, m = (1, :black), leg = :none,
                  xaxis=("Time (s)" ), yaxis = ("neuron",))
	plot!(plt; kwargs...)
end

function raster_plot(pop::SpikeTimit.Spiketimes, order=nothing; mss=1, ax=plot(), c=:black, kwargs...)
    _x, _y, _c = Float32[], Float32[], []
    y0 = Int32[0]
    neurons = order == nothing ? (1:length(pop)) : order
    for (n, cell) in enumerate(neurons)
        for ft in pop[cell]
            push!(_x,ft)
            push!(_y,n)
            !isa(c,Symbol) && (push!(_c, c[cell]))
        end
    end
    c = isa(c,Symbol) ? c : _c
    plt = scatter!(ax, _x , _y, m = (mss, c), msc=c, leg = :none,
                  xaxis=("Time (s)" ), yaxis = ("neuron",); kwargs...)
end