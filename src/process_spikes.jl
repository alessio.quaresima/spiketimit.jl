"""
Unify frequencies bin
Input: spiketimes array with elements of size 620 elements
Return array with sorted spikes in less classes

nfeat	new_bins	Rounding	last_bin
2	    15,5		<--		    2*15=30 <DO> add last to last bin
3	    10,333333	<--		    3*10=30 <DO> add last to last bin
4	    7,75		-->		    4*8=32	<DO>
5	    6,2		    <--		    5*6=30	<DO> add last to last bin
6	    5,16666		<--		    6*5=30	<DO> add last to last bin
7	    4,429		<--		    7*4=28	<DO> add last 3 to last bin
8	    3,875		-->		    8*4=32	<DO>
9	    3,44444		<--		    9*3=27	<DO> add last 4 to last bin
10	    3,1		    <--		    10*3=30	<DO> add last to last bin
11	    2,818181	-->		    11*3=33	<DO>

"""
function resample_spikes!(spiketimes::Vector{Spiketimes}; n_feat = 8, kwargs... )
	for s in 1:length(spiketimes)
		spiketimes[s] = _resample_spikes!(spiketimes[s], n_feat=n_feat)
	end
end

function _resample_spikes!(spiketimes::Spiketimes; n_feat)
    # If we don't reduce the bins
    if n_feat == 1
        return spiketimes
    elseif n_feat > 11 || n_feat < 1
        @assert 1==0 "Impossible resampling values"
    end

	FREQUENCIES = 20
	all_bins = length(spiketimes) ## should be 620

    old_bins = convert(Int64, all_bins/FREQUENCIES) ## should be 31
    @assert (old_bins==31) "WARNING: old_bins != 31, this function is probably broken for other values than 31 (super hardcoded)"
    new_bins   = ceil(Int, old_bins/n_feat )
	new_spikes = Spiketimes(map(x->Vector{Float64}(),1:new_bins*FREQUENCIES))

    for freq in 1:FREQUENCIES
        old_freq = (freq-1)*old_bins
        new_freq = (freq-1)*new_bins
		for old_bin in 1:old_bins
			new_bin  = ceil(Int,(old_bin)/n_feat)
            push!(new_spikes[new_bin+new_freq], spiketimes[old_bin+old_freq]...)
        end
    end
	return new_spikes
end



function transform_into_bursts!(spiketimes::Vector{Spiketimes}; spikes_per_burst_increase=0, kwargs...)
	for s in 1:length(spiketimes)
		_transform_into_bursts!(spiketimes[s], spikes_per_burst_increase=spikes_per_burst_increase)
	end
end


function _transform_into_bursts!(spiketimes::Spiketimes; spikes_per_burst_increase=0)
	##Spike distributions
	# based on plot 1B 0.7 nA (Oswald, Doiron & Maler (2007))
    expdist = Exponential(0.005)
	values = [2,3,4,5,6] .+ spikes_per_burst_increase
	weights = [0.8, 0.15, 0.075, 0.035, 0.03]
   	weights = Weights(weights ./ sum(weights)) # normalized weights
	for neuron in 1:length(spiketimes)
	if !isempty(spiketimes[neuron])
		temp = copy(spiketimes[neuron])
		for spike_time in temp
           	number_of_spikes = sample(values, weights) - 1 # -1 because first spike is determined from data
        	for j in 1:number_of_spikes
        	    interval = rand(expdist)
             	new_time = spike_time + interval + 0.004 # ms
				push!(spiketimes[neuron], new_time)
			end
		end
	end
	sort!(spiketimes[neuron])
end
end


function select_frequencies!(spiketimes::Vector{Spiketimes}; frequencies::Union{Vector, UnitRange{Int64}})
	old_freqs = 1:length(spiketimes[1])
	del_freqs = []
	for freq in old_freqs
		if !(freq ∈  frequencies)
			push!(del_freqs,freq)
		end
	end
	remove_frequencies!(spiketimes, frequencies=del_freqs)
end
function remove_frequencies!(spiketimes::Vector{Spiketimes}; frequencies::Union{Vector, UnitRange{Int64}})
	for s in 1:length(spiketimes)
		for f in frequencies
			spiketimes[s][f] = Vector{Float64}()
		end
	end
end
function _remove_frequencies!(spiketime::Spiketimes; frequencies::Union{Vector, UnitRange{Int64}})
	for f in frequencies
		spiketime[f] .= Vector{Float64}()
	end
end
