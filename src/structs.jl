
##########################
## Words and Phones structs
##########################

Interval = Vector{Float64}
Intervals = Vector{Interval}

struct Phone
	ph::String
	t0::Float64
	t1::Float64
	osc::Array{Float64}
	db::Matrix{Float64}
	function Phone(ph::String, t0::Float64, t1::Float64)
		new(ph, t0, t1, zeros(1), zeros(1,1))
	end
	function Phone(ph::String, t0::Float64, t1::Float64, osc::Vector{Float64}, db::Matrix{Float64})
		new(ph, t0, t1, osc, db)
	end
end

@with_kw struct Word
	word::String = ""
	phones::Vector{Phone} =Vector{Phone}()
	duration::Float64 = -1.
	t0::Float64 = -1.
	t1::Float64 = -1.
end

struct Transcription
	intervals::Vector{Tuple{Float64,Float64}}
	signs::Vector{String}
	function Transcription()
		new([],[])
	end
end

Transcriptions = NamedTuple{(:words, :phones), Tuple{Transcription, Transcription}}

FiringTimes = NamedTuple{(:ft, :neurons), Tuple{Vector{Float64}, Vector{Vector{Int64}}}}

Savepoints  = NamedTuple{(:words , :phones), Tuple{Matrix{Int64}, Matrix{Int64}}}

## Stack spikes together
########################

ms = 1
s = 1000
@with_kw struct InputParams
	n_features::Int = 10 ## Relevant only for the BAE encoding
	spikes_per_burst_increase::Int = 2s
	shift_input::Float64 = 2s
	silence_time::Float64 = 0.1s
	samples::Int = 10
	repetitions::Int = 3
	words::Vector{String} = ["that", "had", "she", "me", "your", "all", "like", "don't", "year", "water", "dark", "rag", "oily", "wash", "ask", "carry", "suit"]
	dialects::Vector{Int} = [1]
	gender::Vector{Char} = ['f','m']
	random_seed::Int = 10
	encoding::String = "bae" #cochlea70 #cochlea35
end

