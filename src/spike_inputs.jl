
"""
Extract all the firing times and the corresponding neurons from an array with
all the neurons and their relative firing times. i.e. the inverse_dictionary
"""
function times_x_neurons(spikes::Spiketimes)
	all_times = Dict{Float64, Vector{Int}}()
	for n in eachindex(spikes)
		if !isempty(spikes[n])
			for tt in spikes[n]
				# tt = round(Int,t*1000/dt) ## from seconds to timesteps
				if haskey(all_times,tt)
					push!(all_times[tt], n)
				else
					push!(all_times, tt=>[n])
				end
			end
		end
	end
	return collect(keys(all_times)), collect(values(all_times))
end


"""
From the inverse_dictionary data structure obtain 2 arrays that are faster to access in the simulation loop.
1. First array contains the sorted spike times.
2. The second array contains vectors with each firing neuron
"""
function sort_spikes(dictionary)
	neurons = Array{Vector{Int}}(undef, length(keys(dictionary)))
	sorted = sort(collect(keys(dictionary)))
	for (n,k) in enumerate(sorted)
		neurons[n] = dictionary[k]
	end
	return Vector{Float64}(sorted), neurons
end

function sort_spikes!(spiketimes::Vector, neurons::Vector{Vector{Int64}})
	order = collect(1:length(spiketimes))
	sort!(order, by=x->spiketimes[x])
	spiketimes[:] .= spiketimes[order]
	neurons[:] = neurons[order]
	return nothing
end

"""
Remove entries with same timestep and merge the firing neuron population
"""
function remove_duplicate_ft!(ft::Vector, neurons::Vector{Vector{Int64}})
	pop_these = []
	for i in 2:length(ft)
		if ft[i] == ft[i-1]
			push!(pop_these,i)
			append!(neurons[i-1], neurons[i])
		end
	end
	for p in sort(pop_these, rev=true)
		popat!(ft,p)
		popat!(neurons,p)
	end
end



"""
Stack together spiketimes sequences:
	- spikes is an array with inputs in the form Vectr
	- durations is the duration in seconds of each encoding

	# assume that all the inputs are not overlapping
"""
function stack_spiketimes(spikes::Vector{Spiketimes}, durations::Vector{Float64}, silence_time::Float64, shift::Float64)
    # for the memory allocation
    nr_unique_fts = 0
    for spike_times in spikes
		tt, nn = times_x_neurons(spike_times)
        nr_unique_fts +=length(tt)
    end
	all_neurons = Vector{Vector{Int}}(undef, nr_unique_fts)
	all_ft = Vector{Float64}(undef, nr_unique_fts)

    global_time = shift
    filled_indices = 0
	for (spike_times, dd) in zip(spikes, durations)
		# get spiketimes
		sorted, neurons = times_x_neurons(spike_times)
		sort_spikes!(sorted, neurons)
		#shift time for each neuron:
		sorted .+= global_time


        ## put them together
        lower_bound = filled_indices +1
        filled_indices = lower_bound + size(sorted,1) -1
        all_ft[lower_bound: filled_indices] = sorted
        all_neurons[lower_bound:filled_indices] = neurons


		global_time += dd
		global_time += silence_time
	end
    @assert(size(all_ft) == size(all_neurons))
	sort_spikes!(all_ft, all_neurons)
	return (ft=all_ft, neurons=all_neurons)
end

function spiketimes2sequence(spike_times::Spiketimes)
	sorted, neurons = _sort_spikes(times_x_neurons(spike_times))
end

function matrix2spiketimes(matrix::BitArray, dt::Float64; get_duration=true)
	spiketimes =Spiketimes()
	duration = size(matrix)[2] * dt
	for r in eachrow(matrix)
		push!(spiketimes,findall(r) .* dt)
	end
	if get_duration
		return spiketimes, duration
	else
		return spiketimes
	end
end

function matrix2sequence(matrix::BitArray, dt)
	return spiketimes2sequence(matrix2spiketimes(matrix, dt, get_duration=false) )
end

function matrix2sequence(matrix::BitArray, dt::Float64, unit::String)
	all_ft, all_neurons = spiketimes2sequence(matrix2spiketimes(matrix, dt, get_duration=false) )
	all_ft_dt = all_ft_to_dt(all_ft, dt, unit)
	return all_ft_dt, all_neurons
end


function spiketimes2sequence(exc_spiketimes::Spiketimes, inh_spiketimes::Spiketimes)
	exc_all_ft, exc_all_neurons = SpikeTimit.spiketimes2sequence(exc_spiketimes)
	inh_all_ft, inh_all_neurons = SpikeTimit.spiketimes2sequence(inh_spiketimes)
	inh_all_ft, inh_all_neurons =  inh_all_ft, .-inh_all_neurons
	return SpikeTimit._sort_spikes(vcat(exc_all_ft,inh_all_ft),vcat(exc_all_neurons,inh_all_neurons))
end




"""
Stack labels sequences:
	- labels is an array with Strings
	- durations is the duration in seconds of each encoding
"""
function stack_labels(labels::Vector{Word}, durations::Vector{Float64}, silence_time::Float64, shift::Float64)
	phones_transcripts = Transcription()
	words_transcripts = Transcription()
	global_time = shift
	for (label, dd) in zip(labels, durations)
		@assert(label.duration == dd)
		push!(words_transcripts.intervals,(global_time, global_time+dd))
		push!(words_transcripts.signs,label.word)
		for ph in label.phones
			push!(phones_transcripts.intervals, (global_time+ph.t0, global_time+ph.t1))
			push!(phones_transcripts.signs,ph.ph)
		end
		global_time += dd + silence_time
	end
	return (words=words_transcripts, phones=phones_transcripts)
end

"""
Extract the word and the phones contained in the datataset and matching with the target word.
Each row addresses a df entry, each row contains all the phones and word labels, with time intervals.
"""
function get_word_labels(df, word::String)
	df_phones = Vector{Vector{Word}}()
	for row in eachrow(df)
		all_phones = Vector{Word}()
		for my_word in eachrow(row.words)
			if my_word[1] == word
				t0,t1 = my_word[2:3]
				word_phones = Vector{Phone}()
				for phone  in eachrow(row.phones)
					if (phone[2] >= t0) && (phone[3]<= t1)
						ph = Phone(phone[1], phone[2]-t0,phone[3]-t0)
						push!(word_phones, ph)
					end
				end
				push!(all_phones, Word(String(my_word[1]), word_phones,t1-t0,t0,t1))
			end
		end
		push!(df_phones, all_phones)
	end
	return df_phones
end

function get_word_spikes(df::DataFrame, words::Union{String, Vector{String}};encoding=encoding)
	if isa(words, String)
		spikes, durations =  get_spikes_in_interval(get_spiketimes(df=df, encoding=encoding), _get_word_interval(df,words))
		return spikes, durations
	else
		spikes = Vector{Spiketimes}()
		durations = Vector{Float64}()
		for word in words
		    _spikes, _durations = get_spikes_in_interval(get_spiketimes(df=df), _get_word_interval(df,word))
			push!(spikes, _spikes...)
			push!(durations, _durations...)
		end
		return spikes, durations
	end
end

"""
For each realization of a word in a dataset entry, extract the interval corresponding to it
Return all the intervals for each dataset entry
"""
function _get_word_interval(df, word::String)
	df_intervals = Vector{Intervals}()
	for row in eachrow(df)
		intervals = Intervals()
		for my_word in eachrow(row.words)
			if my_word[1] == word
				interval = [float(my_word[2]),float(my_word[3])]
				push!(intervals, interval)
			end
		end
		push!(df_intervals, intervals)
	end
	return df_intervals
end

"""
Return the spiketimes subset corresponding to the selected interval, for vectors of Spiketimes
"""
function get_spikes_in_interval(spiketimes::Union{Spiketimes, Array{Spiketimes}},
								df_intervals::Union{Intervals, Array{Intervals}})
	new_spiketimes = Vector{Spiketimes}()
	durations = Vector{Float64}()
	if isa(spiketimes,Spiketimes)
		for intervals in df_intervals
			for interval in intervals
				new_spikes, duration  = _get_spikes_in_interval(spiketimes, interval)
				push!(new_spiketimes, new_spikes)
				push!(durations, duration)
			end
		end
	else
		@assert(length(spiketimes) == length(df_intervals))
		for (spikes, intervals) in zip(spiketimes, df_intervals)
			for interval in intervals
				new_spikes, duration  = _get_spikes_in_interval(spikes, interval)
				push!(new_spiketimes, new_spikes)
				push!(durations, duration)
			end
		end
	end
	return new_spiketimes, durations
end


"""
Return the spiketimes subset corresponding to the selected interval, for one Spiketimes
"""
function _get_spikes_in_interval(spikes, interval)
	new_spiketimes=Spiketimes()
	for neuron in spikes
		neuron_spiketimes = Vector()
		for st in eachindex(neuron)
			if (neuron[st] > interval[1]) && (neuron[st] < interval[end])
				push!(neuron_spiketimes, neuron[st]-interval[1])
			end
		end
		push!(new_spiketimes, neuron_spiketimes)
	end
	duration =interval[end] - interval[1]
	return new_spiketimes, duration
end



function count_words( df::DataFrame, words::Vector{String} )
	counter = Dict{String,Int64}()
    for (i, word) in enumerate(words)
        df_word = find_word(words=word, df=df)
        n_occurences = size(df_word,1)
		push!(counter, word=>n_occurences)
	end
	return counter
end


function count_words(words::Vector{Word})
	return countmap(getfield.(words,:word))
end

## Select and prepare spike inputs for network simulation

function select_words( df::DataFrame, words::Vector{String}; encoding::String,
					samples::Union{Int, Nothing}=nothing,
					kwargs...)
	"""
	Select words in the dataset and prepare them for the simulation
		Parameters:
		==========
		df::DataFrame Input dataset
		words::Vector{String} Words to be selected

		Optional:
		--------
		samples::Int64  Number of samples of the word to be selected in the dataset
		 				if the required samples are more than the available realization
						return an error.

	"""
    all_spikes = Vector{Spiketimes}()
    all_durations = Vector{Float64}()
    all_labels = Vector{Word}()
    for (i, word) in enumerate(words)
        df_word = find_word(words=word, df=df)
        n_occurences = size(df_word,1)
        inds = 1:n_occurences
        (n_occurences <1) &&  (continue)
		if !isnothing(samples)
			if samples <= n_occurences
		        inds = rand(inds, samples)
	        else
		        inds = inds[1:n_occurences]
	            # message = string("WARNING: for word: '", word, "', The required
				# samples per word (", samples, ") exceeds the number of occurences (", n_occurences, ")")
	            # @assert false message
	        end
		end
		###Get intervals and phonemes for each dataset entry (some can have more than one!)
		spiketimes, durations = get_word_spikes(df_word[inds,:], word, encoding=encoding)
		labels = vcat(get_word_labels(df_word[inds,:],word)...)
		@assert(length(spiketimes) == length(labels))

		push!(all_spikes, spiketimes...)
		push!(all_durations, durations...)
		push!(all_labels,labels...)

	end
	return (durations = all_durations, spikes = all_spikes, labels = all_labels)
end


function transcriptions_dt(data::Transcriptions; dt::Float64=0.1)
	return (words=_transcriptions_dt(data.words::Transcription,dt=dt::Float64), phones=
 	_transcriptions_dt(data.phones::Transcription,dt=dt::Float64))
end

function _transcriptions_dt(data::Transcription;dt::Float64)
	_t = Transcription()
	for n in eachindex(data.intervals)
		t0,t1 = data.intervals[n]
		push!(_t.intervals,(round(Int,t0*1000/dt), round(Int, t1*1000/dt)))
		push!(_t.signs,data.signs[n])
	end
	return _t
end

function ft_dt(data::FiringTimes; dt=0.1::Real, unit::String="s")
	if unit=="s"
		x = 1000/dt
	elseif unit=="ms"
		x = 1/dt
	else
		throw(DomainError(unit, "Valid units are 's' and 'ms'"))
	end
	ft = Vector{Int}()
	for s in eachindex(data.ft)
		push!(ft,round(Int,data.ft[s]*x))
	end
	neurons = copy(data.neurons)
	remove_duplicate_ft!(ft,neurons)
	return (ft=ft, neurons=neurons)
end



function mix_inputs(samples::Int64, repetitions::Int64)
	return shuffle(repeat(1:samples, repetitions))
end

function get_ordered_spikes(word_inputs::NamedTuple;
	 			repetitions::Int64 = -1, silence_time::Real, shift::Real, ids::Vector{Int64}=[], unit="s")
	if unit=="s"
		x = 1000
	elseif unit=="ms"
		x = 1
	end
	if isempty(ids)
		if repetitions < 0
			throw(error("Define 'repetitions' per sample or set the 'ids' variable "))
		end
		ids = mix_inputs(length(durations), repetitions)
	end

	@unpack spikes, durations, labels = word_inputs
	ordered_spikes = stack_spiketimes(spikes[ids], durations[ids], silence_time/x, shift/x)
	transcriptions =SpikeTimit.stack_labels(labels[ids],durations[ids],silence_time/x, shift/x)
	return ordered_spikes, transcriptions
end

function _get_savepoints(trans::Transcription, n_measure::Int)
	measures = Array{Int64,2}(undef, size(trans.intervals,1), n_measure)
	for (i,step) in enumerate(trans.intervals)
		l = step[2] - step[1]
		l_single = floor(Int, l/n_measure)
		measures[i,1:n_measure] = (step[1] .+ collect(1:n_measure).* l_single)
		# push!(measures,step[1] .+ collect(1:n_measure).* l_single)
	end
	return measures
end

function get_savepoints(trans::Transcriptions; per_word::Int, per_phone::Int)
	w = _get_savepoints(trans.words, per_word)
	p = _get_savepoints(trans.phones, per_phone)
	return (words= w , phones=p)
end
#
# """
# Get firing times for each phones list.
# The phones list contains all the relevant phones (and their time interval) in a certain df entry
# """
# function get_phones_spiketimes(spikes, phones_list)
# 	phone_spiketimes = []
# 	phone_labels = []
# 	for (phones, spike) in zip(phones_list, spikes)
# 		intervals=[]
# 		for phone in phones
# 			push!(intervals,phone[2:3])
# 			push!(phone_labels,phone[1])
# 		end
# 		spiketime, duration = SpikeTimit.get_spikes_in_interval(; spiketimes=spike,intervals=intervals)
# 		push!(phone_spiketimes,spiketime)
# 	end
# 	return phone_labels, vcat(phone_spiketimes...)
# end


function get_encoding_dimension(spikes::Vector{Spiketimes})
	return length(spikes[1])
end
