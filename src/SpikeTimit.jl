module SpikeTimit

    using DataFrames
    using DataFramesMeta
    using OrderedCollections
    using Random
    using StatsBase
    using Distributions
	using UnPack
	using MAT
	using NPZ
	using Parameters

	import Plots: Series, Plot, Subplot
	using Plots
	using ColorSchemes

    ## This is the sample rate used in the scripts.
    sr = 16000
	s = 1000
	ms = 1
    ## This is the rescaling factor for the spike-time discussed in the PDF
    correction = 5.
	##
	# Spiketimes{T} = Vector{Vector{<:Real}}
	Spiketimes = Vector{Vector{Float64}}
	make_spiketimes(n) = [Vector{Float64}() for _ in 1:n]

	include("structs.jl")
	include("import_dataset.jl")
	include("process_spikes.jl")
	include("spike_inputs.jl")
	include("raster.jl")
	export SpikeTimes

end
