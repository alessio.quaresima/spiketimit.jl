"""
Get words and time intervals from the SenID
"""
function _get_words(root, senID)
  path = joinpath(root, senID*".wrd")
  times0 = []
  times1 = []
  words = []
  for line in readlines(path)
      t0,tf,w = split(line)
      push!(times0,parse(Int,t0))
      push!(times1,parse(Int,tf))
      push!(words, String(w))
  end
  _data = Array{Union{String,Float64},2}(undef,length(times0),3)
  _data[:,1] = words
  _data[:,2] = times0 ./ sr
  _data[:,3] = times1 ./ sr
  return _data
end
matopen

"""
Get phonemes and time intervals from the SenID
"""
function _get_phones(root, senID)
  path = joinpath(root, senID*".phn")
  times0 = []
  times1 = []
  phones = []
  for line in readlines(path)
      t0,tf,p = split(line)
      push!(times0,parse(Int,t0))
      push!(times1,parse(Int,tf))
      push!(phones, String(p))
  end
  _data = Array{Union{String,Float64},2}(undef,length(times0),3)
  _data[:,1] = phones
  _data[:,2] = times0 ./ sr
  _data[:,3] = times1 ./ sr
  return _data
end


# 	function create_speaker_dataset(;dir)
# df = DataFrame(dir)
# dir = "/home/cocconat/Documents/Research/phd_project/speech/litwin-kumar_model_thesis/DOC/SPKRINFO.TXT"
# using DataFrames
# DataFrames.readtable(dir)
# # , allowcomments=true, commentmark='%')

function _get_dialect(root)
	return splitpath(root) |> x->parse(Int,filter(startswith("dr"),x)[1][end])
end

function create_dataset(;dir)
    df = DataFrame(speaker = String[] , senID = String[], dialect=Int[], gender=Char[], path=String[], words=Array{Union{String, Float64},2}[], phones=Array{Union{String, Float64},2}[], sentence=Vector{String}[])
    for (root, dirs, files) in walkdir(dir)
        for file in files
            if endswith(file,"wav")
                speaker = splitpath(root)[end]
                senID   = split(file,".")[1]
                words   = _get_words(root, senID)
                phones  = _get_phones(root, senID)
	    		dr = _get_dialect(root)
	    		gender = speaker[1]
				sentence = String.(words[:,1])
                push!(df,(speaker,senID,dr,gender,joinpath(root,senID),words,phones,sentence))
            end
        end
    end
    return sort!(df)
end

## Create a dictionary with all words in the dataset
function create_dictionary(;file)
    dict = OrderedDict()
    for line in readlines(file)
        if !startswith(line, ";")
            word, sounds = split(line,"/")
            push!(dict, strip(word)=>sounds)
        end
    end
    return dict
end

## Obsolete. Use MetaFrame to subselect the dataset.
function find_word(;df::DataFrame, words::Union{Vector{String}, String})
	words = isa(words, String) ? [words] : words
	in_words(df_words) = !isempty(intersect(Set(df_words),Set(words)))
    """Search for word in the dataset and return the items"""
    return @where(df, in_words.(:words))
end

########################
## Extract .mat files ##
########################

function _get_matrix(;df::DataFrame)
    get_matrix_file(file) =  file*"_binary_matrix.mat"
    return get_matrix_file.(df.path)
end

function _get_file(file, ext)
    return file*"."*ext
end


function get_spikes_bae(;df::DataFrame)
	get_spiketimes_file(file)= file*"_spike_timing.mat"
	get_array(value::Float64) = begin x=zeros(Float64, 1); x[1] = value; x end
	spikes =[]
	for f in df.path
		fp = matopen(get_spiketimes_file(f))
		spike = read(fp)|> x->get(x,"spike_output", nothing)[1,:]
		map(row->spike[row] = spike[row][:], findall(typeof.(spike) .==Array{Float64,2}))
		map(row->spike[row] = get_array(spike[row]), findall(typeof.(spike) .==Float64))
		@assert length(spike) == length(findall(x-> isa(x, Vector{Float64}), spike))
		push!(spikes,spike .* correction)
		close(fp)
	end
	return map(x->Spiketimes(x), spikes)
end
word = "/home/cocconat/Documents/Research/phd_project/speech/datasets/Spike TIMIT/train/dr1/fcjf0"


function get_spikes_cochlea(;df::DataFrame, encoding::Int)
	!(encoding in [35,70]) && throw(ArgumentError("Encoding must be 35 or 70"))
	get_spiketimes_file(file)= file*"_cochlea$encoding.npz"
	spikes=[]
	for f in df.path
		data = npzread(get_spiketimes_file(f))["arr_0"]
		_spiketimes=make_spiketimes(encoding)
		for (t,n) in eachcol(data)
			push!(_spiketimes[round(Int,n)+1],t)
		end
		push!(spikes,_spiketimes)
	end
	return map(x->Spiketimes(x), spikes)
end

function get_spiketimes(;df, encoding::String)
	if encoding == "bae"
		return get_spikes_bae(;df=df)
	elseif startswith(encoding,"cochlea")
		encoding = replace(encoding,"cochlea"=>"")
		isempty(encoding) && throw(ArgumentError("Define cochlea encoding: 35 or 70"))
		return get_spikes_cochlea(;df=df, encoding=parse(Int,encoding))
    elseif encoding=="another encoding"
	else
		throw(ArgumentError("Encoding must be 'cochlea(35|70) or bae'"))
	end
end

function py2j_words(words)
    jwords = Vector{Word}
    for word in words
        phs = []
        for ph in word.phones
            push!(phs,Phone(ph.ph, ph.t0, ph.t1, Array{Float64}(ph.osc), Matrix{Float64}(ph.db)))
        end
        push!(jwords,Word(word.word, phs, word.duration, word.t0, word.t1))
    end
    return jwords
end
